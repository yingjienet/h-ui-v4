/* =======================================================================
 * jQuery.Huitotop.js v3.0 返回顶部
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2022.09.13
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
(function($) {
  $.Huitotop = function(options) {
    var settings = $.extend({
      bottom: 60
    }, options);

    var $toTop = $('<a href="javascript:void(0)" class="hui-tools-right hui-totop hui-iconfont" title="返回顶部" alt="返回顶部" style="display:none; bottom: ' + settings.bottom + 'px">&#xe684;</a>');
    $('body').append($toTop);

    $toTop.on('click', function() {
      $("html, body").animate({
        scrollTop: 0
      }, 120);
    });

    function backToTopFun() {
      var st = $(document).scrollTop();
      var winh = $(window).height();
      if (st > 0) {
        $toTop.fadeIn();
      } else {
        $toTop.fadeOut();
      }
    }

    function debounce(func, wait) {
      var timeout;
      return function() {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function() {
          func.apply(context, args);
        }, wait);
      };
    }

    var debouncedBackToTop = debounce(backToTopFun, 100);
    $(window).on("scroll", debouncedBackToTop);
  };
})(jQuery);