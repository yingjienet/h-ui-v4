/* =======================================================================
 * jQuery.format.js 金额格式化
功能和特点：
1、字符串格式化：将长字符串按照指定的步长分割，并用分隔符连接。
2、灵活的分隔符：用户可以指定任意字符作为分隔符。
3、处理任意字符串：函数接受任意字符串，并将其转换为字符串格式（如果可能）。

使用方法：
在 jQuery 环境中，你可以通过调用 $.format(str, step, splitor) 来使用这个函数，其中：
· str 是需要格式化的原始字符串。
· step 是每个分段的长度。
· splitor 是用来连接分段的分隔符。
例子：
var formattedString = $.format("1234567890", 3, ',');
console.log(formattedString);  // 输出 "123,456,789,0"
这个插件可以用于电话号码、身份证号、银行卡号等需要格式化显示的场景。
* ========================================================================*/
!function($) {
  $.extend({
    format: function(str, step, splitor) {
      str = str.toString();
      var len = str.length;

      if (len > step) {
        var l1 = len % step,
        l2 = parseInt(len / step),
        arr = [],
        first = str.substr(0, l1);
        if (first != '') {
          arr.push(first);
        };
        for (var i = 0; i < l2; i++) {
          arr.push(str.substr(l1 + i * step, step));
        };
        str = arr.join(splitor);
      };
      return str;
    }
  });
} (window.jQuery);