/* =======================================================================
 * jQuery.Huimodalalert.js v2.2 提示对话框
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.13
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 功能介绍：当用户将鼠标悬停在小图上时，会显示一个更大的图片预览。
 * 使用方法：
  $("body").Huimodalalert(
    {
      content: '我是消息框，2秒后我自动滚蛋！',
      speed: 2000,
    }, function () {
    alert("我已滚蛋");
    }
  );
 * ========================================================================*/
!function ($) {
  $.fn.Huimodalalert = function (options, callback) {
    var defaults = {
      type: "info",
      btn: ["确定"],
      content: "弹窗内容",
      speed: 0, // 默认值应该是数字类型
      area: ["400", "auto"]
    };
    options = $.extend({}, defaults, options); // 避免修改原始的 defaults 对象

    // 确保 speed 是有效的数字
    if (typeof options.speed !== "number" || isNaN(options.speed)) {
      options.speed = defaults.speed;
    }

    function createModal(options) {
      var modalIcon = '<i class="hui-iconfont mr-5">&#xe6e0;</i>';
      var modalClassName = "hui-modal-alert-info";
      switch (options.type) {
        case "success":
          modalClassName = "hui-modal-alert-success";
          modalIcon = '<i class="hui-iconfont mr-5">&#xe6e1;</i>';
          break;
        case "error":
          modalClassName = "hui-modal-alert-error";
          modalIcon = '<i class="hui-iconfont mr-5">&#xe60b;</i>';
          break;
      }

      var w = options.area[0] === "auto" ? "400px" : options.area[0] + "px";
      var l = options.area[0] === "auto" ? "-200px" : "-" + (options.area[0] / 2) + "px";
      var h = options.area[1] === "auto" ? "auto" : options.area[1] + "px";

      var htmlstr = `
        <div id="hui-modal-alert" class="hui-modal hui-modal-alert radius" style="width:${w};height:${h};margin-left:${l}">
          <div class="${modalClassName}">${modalIcon}${options.content}</div>
          <div class="hui-modal-footer">
            <button class="hui-btn hui-btn-primary radius">${options.btn[0]}</button>
          </div>
        </div>
        <div id="hui-modal-mask" class="hui-modal-mask"></div>
      `;

      if ($("#hui-modal-alert").length > 0) {
        $("#hui-modal-alert,#hui-modal-mask").remove();
      }
      $("body").addClass("hui-modal-open").append(htmlstr);
      var $modal = $("#hui-modal-alert");
      $modal.addClass(modalClassName);
      if (options.speed === 0) {
        $modal.show();
      } else {
        $modal.fadeIn().delay(options.speed).fadeOut(function () {
          removeModal();
        });
      }
    }

    function removeModal() {
      $("#hui-modal-alert,#hui-modal-mask").remove();
      $("body").removeClass("hui-modal-open");
      if (typeof callback === "function") {
        callback();
      }
    }

    function bindButtonEvents() {
      $(".hui-modal-footer .hui-btn").on("click", function () {
        removeModal();
      });
    }

    this.each(function () {
      createModal(options);
      bindButtonEvents();
    });

    return this; // Maintain jQuery chainability
  };
}(window.jQuery);