/* =======================================================================
 * jQuery.Huimarquee.js 水平滚动，跑马灯效果
 * ========================================================================*/
!function($) {
  $.fn.Huimarquee = function(options) {
    var defaults = {
      height: 22,
      speed: 3000,
      delay: 30
    }
    var settings = $.extend(defaults, options);
    this.each(function() {
      var that = $(this);
      if (that.find(".hui-marquee-content").height() > settings.height) {
        var pause = false;
        var scrollT;
        that.append(that.html());
        that.mouseover(function() {
          pause = true;
        })
        that.mouseout(function() {
          pause = false;
        })
        that.scrollTop(0);
        var start = function() {
          scrollT = setInterval(scrolling, settings.speed);
          if (!pause) {
            that.scrollTop(that.scrollTop() + 2);
          }
        }
        var scrolling = function() {
          if (that.scrollTop() % settings.height != 0) {
            Sthat.scrollTop(that.scrollTop() + 2);
            if (that.scrollTop() >= that.height() / 2) {
              that.scrollTop(0)
            }
          } else {
            clearInterval(scrollT);
            setTimeout(start, settings.delay);
          }
        }
        setTimeout(start, settings.delay);
      };
    })
  }
} (window.jQuery);