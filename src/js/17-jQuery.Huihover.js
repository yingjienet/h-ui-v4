/* =======================================================================
 * jQuery.Huihover.js v2.1 hover效果
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
!function($) {
  $.fn.Huihover = function(options) {
    // 确保options是一个对象
    options = (typeof options === 'object' && options !== null) ? options : {};

    var defaults = {
      className: "hover",
      callback1: $.noop, // 默认回调函数
      callback2: $.noop  // 默认回调函数
    }

    var settings = $.extend({}, defaults, options);

    this.each(function() {
      var that = $(this);
      that.hover(
        function() {
          that.addClass(settings.className);
          if (typeof settings.callback1 === "function") {
            settings.callback1.call(that);
          }
        },
        function() {
          that.removeClass(settings.className);
          if (typeof settings.callback2 === "function") {
            settings.callback2.call(that);
          }
        }
      );
    });

    // 返回jQuery对象以支持链式调用
    return this;
  };
}(window.jQuery);