/* =======================================================================
 * jQuery.Huifocusblur.js v2.1 得到失去焦点
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 使用方法：
   $(".hui-input-text,.hui-textarea").Huifocusblur();
 * ========================================================================*/
!function($) {
  $.fn.Huifocusblur = function(options){
    // 确保options是一个对象
    options = (typeof options === 'object' && options !== null) ? options : {};

    var defaults = {
      className: "focus",
    }
    
    var settings = $.extend({}, defaults, options);
    this.each(function(){      
      var that = $(this);
      that.focus(function() {
        that.addClass(settings.className).removeClass("inputError");
      });      
      that.blur(function() {
        that.removeClass(settings.className);
      });
    });

    // 支持链式调用
    return this;
  }
} (window.jQuery);