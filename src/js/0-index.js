/*-----------H-ui前端框架-------------
* H-ui.min.js v4.0.0
* http://www.h-ui.net/
* Created & Modified by guojunhui
* Date modified 2024.09.11
*
* Copyright 2013-2022 郭俊辉 All rights reserved.
* Licensed under MIT license.
* http://opensource.org/licenses/MIT
*/
/*
Includes：
01-jQuery.IEMobileHack.js v1.0
02-jQuery.cookie.js v1.4.1
03-jQuery.form.js v3.51.0
04-jQuery.lazyload.js v1.9.3
05-jQuery.responsive-nav.js v1.0.39
06-jQuery.placeholder.js
07-jQuery.emailsuggest.js v1.0
08-jQuery.format.js
09-jQuery.iCheck.js
10-jQuery.raty.js v2.4.5
11-jQuery.onePageNav.js
12-jQuery.stickUp.js
13-jQuery.ColorPicker.js

14-jQuery.HuiaddFavorite.js
15-jQuery.Huisethome.js
16-jQuery.Huisidenav.js
17-jQuery.Huihover.js v2.0
18-jQuery.Huifocusblur.js V2.0
19-jQuery.Huiselect.js
20-jQuery.Huitab.js v2.0.1
21-jQuery.Huifold.js v2.0
22-jQuery.Huitags.js v2.0
23-jQuery.Huitagsmixed.js
24-jQuery.Huitextarealength.js v2.0
25-jQuery.Huipreview.js v2.1
26-jQuery.Huimodalalert.js v1.0
27-jQuery.Huialert.js
28-jQuery.Huitotop.js v2.0
29-jQuery.Huimarquee.js
30-jQuery.Huispinner.js v2.0
31-jQuery.Huiloading.js v1.0
32-jQuery.HuicheckAll.js v1.0
33-jQuery.HuitogglePassword.js v1.0
34-jQuery.Huimodaltips v1.0

35-Bootstrap.button.js v3.3.0
36-Bootstrap.modal.js v3.3.0
37-Bootstrap.dropdown.js v3.3.0
38-Bootstrap.transition.js v3.3.0
39-Bootstrap.tooltip.js v3.3.0
40-Bootstrap.popover.js v3.3.0
41-Bootstrap.slider.js v1.0.1
42-Bootstrap.datetimepicker.js
43-Bootstrap.Switch v1.3
44-Bootstrap.alert.js v3.3.0
*/