/* =======================================================================
 * jQuery.Huimodaltips.js v1.1 返回顶部
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.13
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
(function($) {
  $.Huimodaltips = function(options, callback) {
    var defaults = {
      btn: ['确定'],
      content: '弹窗内容',
      title: '提示',
      icon: '',
      width: 400
    };
    var settings = $.extend({}, defaults, options);

    var w = settings.width === 'auto' ? '400px' : settings.width + 'px';
    var htmlstr = '<div id="hui-modal-tips" class="hui-modal hide hui-modal-tips">' +
                   '<div class="hui-modal-dialog" style="width:' + w + '">' +
                     '<div class="hui-modal-content radius">' +
                       '<div class="hui-modal-header">' +
                         '<div class="hui-modal-title">' + settings.title + '</div>' +
                       '</div>' +
                       '<div class="hui-modal-body">' +
                         '<div class="hui-modal-tips-icon">' + settings.icon + '</div>' +
                         '<div class="hui-modal-tips-content">' + settings.content + '</div>' +
                         '<div class="hui-modal-btn-wrapper">' +
                           settings.btn.join('').replace(/,/g,'</button><button class="hui-btn hui-btn-success size-M radius">') +
                         '</div>' +
                       '</div>' +
                     '</div>' +
                   '</div>' +
                 '</div>';

    if ($("#hui-modal-tips").length > 0) {
      $("#hui-modal-tips").remove();
    }
    $('body').append(htmlstr);
    $("#hui-modal-tips").modal("show");

    $("#hui-modal-tips .hui-modal-btn-wrapper .hui-btn").on("click", function(){
      $("#hui-modal-tips").modal("hide");
      if (callback && typeof callback === "function") {
        callback();
      }
    });
  };
}(jQuery));
