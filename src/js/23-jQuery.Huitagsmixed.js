/* =======================================================================
 * jQuery.Huitagsmixed.js v1.1 标签混排效果
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 使用方法：
  $('.my-elements').Huitagsmixed();
 * ========================================================================*/
!function($) {
  $.fn.Huitagsmixed = function() {
    return this.each(function() {
      // 定义随机数范围
      var x = 9;
      var y = 0;
      // 生成随机数并添加类名
      var rand = parseInt(Math.random() * (x - y + 1) + y);
      $(this).addClass("tags" + rand);
    });
  }
} (window.jQuery);