/* =======================================================================
 * jQuery.Huisethome.js v2.0 设为浏览器首页
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 *
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 使用方法 $.Huisethome() 或者在 jQuery 对象上调用.Huisethome() 来使用这个插件。
 * <button class="homepageSetter">设置主页</button>
 * $(".homepageSetter").Huisethome({ site: "https://www.example.com" });
 * ========================================================================*/
!function($) {
  $.Huisethome = function(options) {
    // 确保options是一个对象
    options = (typeof options === 'object' && options !== null) ? options : {};

    var webSite = options.site || window.location.href;
    // 验证网站地址是否有效
    if (!webSite) {
      console.error("Huisethome: 网站地址无效");
      return;
    }
    try {
      if (window.external && window.external.setHomePage) {
        window.external.setHomePage(webSite);
      } else {
        throw new Error('Browser does not support setting home page programatically.');
      }
    } catch (error) {
      // 检测浏览器和操作系统
      var isMacOS = navigator.platform.indexOf('Mac') !== -1;
      var isSafari = /^((?!chrome).)*safari/i.test(navigator.userAgent);
      var isChrome = /chrome/i.test(navigator.userAgent);
      var message = "此操作被浏览器拒绝！";

      if (isMacOS) {
        if (isSafari) {
          message += "\n请手动设置：\n1. 打开 Safari 浏览器。\n2. 点击屏幕左上角的 'Safari' 菜单。\n3. 选择 '偏好设置'。\n4. 在 '通用' 标签页中，找到 '主页' 栏。\n5. 输入或更改网页地址。";
        } else if (isChrome) {
          message += "\n请手动设置：\n1. 打开 Chrome 浏览器。\n2. 点击屏幕右上角的三个点（菜单按钮）。\n3. 选择 '设置'。\n4. 在 '启动时' 部分，点击 '打开特定页面或一组页面'。\n5. 点击 '添加新页面' 并输入您想要设置为主页的网址。";
        } else {
          message += "\n请根据您的浏览器类型手动设置主页。";
        }
      } else {
        message += "\n请在浏览器地址栏输入\"about:config\"并回车\n然后将 [signed.applets.codebase_principal_support]的值设置为'true',双击即可。";
      }

      // 判断 $.Huimodalalert 是否存在
      if (typeof $.Huimodalalert === 'function') {
        $.Huimodalalert(message, 2000);
      } else {
        alert(message);
      }
    }
  };

  // 可以提供一个快捷方式，以便在 jQuery 对象上调用
  $.fn.Huisethome = function(options) {
    // 调用静态方法
    $.Huisethome(options);
  };
}(window.jQuery);
