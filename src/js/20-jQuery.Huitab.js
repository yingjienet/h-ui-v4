/* =======================================================================
 * jQuery.Huitab.js v2.1 选项卡
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 使用方法：
  $("#HuiTab-demo1").Huitab({
    tabBar: ".hui-tab-header span",
    tabCon: ".hui-tab-content",
    className: "current",
    tabEvent: "click",
    index: 0
  });
 * ========================================================================*/
!function($) {
  $.fn.Huitab = function(options, callback){
    // 确保options是一个对象
    options = (typeof options === 'object' && options !== null) ? options : {};

    var defaults = {
      tabBar: ".hui-tab-header span",
      tabCon: ".hui-tab-content",
      className: "current",
      tabEvent: "click",
      index: 0,
    }
    var settings = $.extend({}, defaults, options);

    return this.each(function(){
      var $this = $(this);
      var $tabBar = $this.find(settings.tabBar);
      var $tabCon = $this.find(settings.tabCon);

      $tabBar.removeClass(settings.className).eq(settings.index).addClass(settings.className);
      $tabCon.hide().eq(settings.index).show();
      
      $tabBar.on(settings.tabEvent,function() {
        var $thisTab = $(this);
        $tabBar.removeClass(settings.className);
        $thisTab.addClass(settings.className);

        var index = $tabBar.index($thisTab);
        $tabCon.hide().eq(index).show();

        if (callback) {
          callback();
        }
      });

      // 销毁时解绑事件
      $this.data('Huitab', { destroy: function() {
        $tabBar.off(settings.tabEvent);
      }});
    });
  }
} (window.jQuery);