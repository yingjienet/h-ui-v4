/* =======================================================================
 * jQuery.HuitogglePassword.js v2.1 隐藏显示密码
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2017.05.05
 * Copyright 2017 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 调用方法：
$(document).ready(function() {
  $('#yourPasswordInput').HuitogglePassword({
    event: "mouseover", // 可以设置为你想要的事件类型
    toggleClass: "custom-toggle-class", // 可以设置为你的自定义类名
    iconHtml: '<span class="' + "custom-toggle-class" + '"></span>' // 可以设置为你的自定义图标 HTML
  });
});
 * ========================================================================*/
!function($) {
  $.fn.HuitogglePassword = function(options) {
    var settings = $.extend({
      // 可以在这里添加更多的默认配置项
      event: "click", // 默认的事件类型
      toggleClass: "toggle-password", // 切换图标的类名
      iconHtml: '<span class="' + this.toggleClass + '"></span>' // 默认的图标 HTML
    }, options );

    this.each(function() {
      var $input = $(this);
      var $toggle = $(settings.iconHtml).insertBefore($input);

      // 确保只对密码输入框应用这个功能
      if ($input.prop('type') === 'password') {
        $toggle.on(settings.event, function() {
          if ($input.prop('type') === 'password') {
            $input.prop('type', 'text');
            $toggle.addClass('active');
          } else {
            $input.prop('type', 'password');
            $toggle.removeClass('active');
          }
        });

        // 初始化时隐藏密码
        $input.prop('type', 'password');
      }
    });

    // 可以在这里添加更多的初始化代码
  };
} (window.jQuery);