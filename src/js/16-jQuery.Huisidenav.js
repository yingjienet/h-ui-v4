/* =======================================================================
 * jQuery.Huisidenav.js v2.0 左侧菜单-隐藏显示
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/

function Huisidenav(trigger) {
  // 验证 trigger 参数是否有效
  if (!trigger) {
    console.error("Huisidenav: trigger 参数无效");
    return;
  }
  // 存储 jQuery 对象引用，减少重复选择
  var $trigger = $(trigger);
  var $body = $("body");

  // 确保 $trigger 是一个有效的 jQuery 对象
  if ($trigger.length === 0) {
    console.error("Huisidenav: 无法找到对应的 trigger 元素");
    return;
  }

  // 切换 'open' 类和 'big-page' 类
  $trigger.toggleClass("open");
  $body.toggleClass("big-page");
}