/* =======================================================================
 * jQuery.Huitotop.js v3.0 处理文件上传
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2022.09.13
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * 调用方法：
$(document).ready(function() {
  $("#your-element").Huiuoload({
    url: '/upload', // 服务器端处理上传的URL
    beforeSend: function($fileInput) {
      // 显示上传前的提示信息
      $('#upload-status').text('正在上传...').show();
    },
    progress: function(percentComplete, $fileInput) {
      // 更新进度条
      $('#progress-bar').width(percentComplete * 100 + '%');
    },
    success: function(response, $fileInput) {
      // 隐藏进度条和状态信息
      $('#progress-bar').width(0);
      $('#upload-status').text('上传成功！').show().fadeOut(3000);
    },
    error: function(xhr, status, error, $fileInput) {
      // 处理上传错误
      var errorMessage = '上传失败，请稍后再试。';
      if (xhr.status === 0) {
        errorMessage = '未连接到网络。';
      } else if (xhr.status === 404) {
        errorMessage = '上传的文件未找到。';
      } else if (xhr.status === 500) {
        errorMessage = '服务器内部错误。';
      } else if (error) {
        errorMessage = error;
      }
      $('#upload-status').text(errorMessage).show();
    }
  });
});
 * ========================================================================*/
(function($) {
  function Huiuoload(options) {
    var settings = $.extend({
      url: '', // 上传文件的服务器端URL
      method: 'POST', // 上传方法，默认为POST
      dataType: 'json', // 预期服务器返回的数据类型
      beforeSend: function() {}, // 上传前的回调函数
      success: function() {}, // 上传成功的回调函数
      error: function() {}, // 上传失败的回调函数
      progress: function() {} // 上传进度的回调函数
    }, options);

    this.on('change', '.input-file', function() {
      var $fileInput = $(this);
      var $uploadUrl = $fileInput.closest('.upload-url');
      var file = this.files[0]; // 获取选择的文件

      if (file) {
        // 创建 FormData 对象
        var formData = new FormData();
        formData.append('file', file);

        // 调用 AJAX 上传文件
        $.ajax({
          url: settings.url,
          type: settings.method,
          data: formData,
          dataType: settings.dataType,
          contentType: false, // 不设置内容类型
          processData: false, // 不处理数据
          beforeSend: function() {
            settings.beforeSend.call(this, $fileInput);
          },
          xhr: function() {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function(evt) {
              if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                settings.progress.call(this, percentComplete, $fileInput);
              }
            }, false);
            return xhr;
          },
          success: function(response) {
            settings.success.call(this, response, $fileInput);
          },
          error: function(xhr, status, error) {
            settings.error.call(this, xhr, status, error, $fileInput);
          }
        });
      }

      // 更新显示上传文件路径的元素
      $uploadUrl.val($fileInput.val()).focus().blur();
    });

    return this;
  }

  $.fn.Huiuoload = Huiuoload;

})(jQuery);
