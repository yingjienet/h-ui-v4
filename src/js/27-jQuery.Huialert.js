/* =======================================================================
 * jQuery.Huialert.js v1.0 警告框组件
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.13
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * ========================================================================*/
(function($) {
  function Huialert() {
    var $wrappers = $('.hui-alert');

    // 检查是否存在 Huihover 函数，如果存在则调用
    if ($.isFunction($.fn.Huihover)) {
      $wrappers.find('i').Huihover();
    }

    $wrappers.on('click', 'i', function() {
      var $this = $(this);
      var alertWrapper = $this.closest('.hui-alert');
      alertWrapper.fadeOut('normal', function() {
        alertWrapper.remove();
      });
    });
  }

  // 检查是否在文档加载完毕后执行
  $(document).ready(function() {
    Huialert();
  });
})(window.jQuery);