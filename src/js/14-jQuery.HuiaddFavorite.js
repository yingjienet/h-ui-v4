/* =======================================================================
 * jQuery.HuiaddFavorite.js v1.0 添加收藏
 * http://www.h-ui.net/
 * Created & Modified by guojunhui
 * Date modified 2024.09.12
 *
 * Copyright 2017-2024 郭俊辉 All rights reserved.
 * Licensed under MIT license.
 * http://opensource.org/licenses/MIT
 * <a title="收藏本站" href="javascript:;" onClick="HuiaddFavorite('H-ui前端框架','http://www.h-ui.net/');">收藏本站</a>
 * ========================================================================*/

function HuiaddFavorite(options) {
  options.site = options.site || window.location.href;
  options.name = options.name || document.title;

  // 尝试添加到收藏夹
  try {
    if (window.external && window.external.addFavorite) {
        window.external.addFavorite(options.site, options.name);
    } else if (window.sidebar && window.sidebar.addPanel) {
        window.sidebar.addPanel(options.name, options.site, "");
    } else {
        throw new Error('Browser does not support adding favorites programatically.');
    }
  } catch (error) {
    // 显示用户友好的提示信息
    if (typeof $.Huimodalalert === 'function') {
      $.Huimodalalert("加入收藏失败，请使用Ctrl+D进行添加", 2000);
    } else {
      alert("加入收藏失败，请使用Ctrl+D进行添加");
    }
  }
}
